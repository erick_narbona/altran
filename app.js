const express = require("express");
const app = express();

const clientsRoute = require("./routes/clients");

const policiesRoute = require("./routes/policies");

app.use(clientsRoute);

app.use(policiesRoute);

app.use(function(req, res, next) {
  res.status(404).send("not exist");
});

app.listen(3000);

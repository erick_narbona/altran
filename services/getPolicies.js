const httpRequest = require("../libs/httpRequest");
exports.get = next => {
  httpRequest.get(
    "http://www.mocky.io/v2/580891a4100000e8242b75c5",
    (err, response) => {
      if (err) return next(err, null);
      next(null, response.policies);
    }
  );
};

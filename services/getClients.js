const httpRequest = require("../libs/httpRequest");
exports.get = next => {
  httpRequest.get(
    "http://www.mocky.io/v2/5808862710000087232b75ac",
    (err, response) => {
      if (err) return next(err, null);
      next(null, response.clients);
    }
  );
};

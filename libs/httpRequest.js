const https = require("http");
exports.get = (url, callback) => {
  https
    .get(url, response => {
      let data = "";
      response.on("data", d => {
        data += d;
      });
      response.on("end", () => {
        callback(null, JSON.parse(data));
      });
    })
    .on("error", function(err) {
      callback(err, null);
    });
};

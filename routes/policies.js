const getClients = require("../services/getClients").get;
const getPolicies = require("../services/getPolicies").get;
const express = require("express");
const _ = require("underscore");
const router = express.Router();

// require id of user admin send by query string
// /api/v1/policies/john?authUserId=12345678-444-444444
router.get("/api/v1/policies/client/:name", (req, res, next) => {
  try {
    getClients(function(err, response) {
      if (err) return res.send({ error: err });
      const user = _.filter(response, function(o) {
        return o.id === req.query.authUserId;
      })[0];
      if (!user) return res.send({ error: "unautorized" });
      if (user.role !== "admin") return res.send({ error: "unautorized" });

      const client = _.filter(
        response,
        o =>
          o.name.toLowerCase().indexOf(req.params.name.toLocaleLowerCase()) >= 0
      )[0];

      getPolicies((err, response) => {
        if (err) return res.send({ error: err });
        const policie = _.filter(response, o => o.clientId === client.id);
        res.send(policie);
      });
    });
  } catch (err) {
    next(err);
  }
});

// require id of user admin send by query string
// /api/v1/policies/john?authUserId=12345678-444-444444
router.get("/api/v1/policies/:policiesId", (req, res, next) => {
  try {
    getClients((err, response) => {
      if (err) return res.send({ error: err });
      const authUser = _.filter(
        response,
        o => o.id === req.query.authUserId
      )[0];
      if (!authUser || authUser.role !== "admin")
        return res.send({ error: "unautorized" });
      getPolicies((err, response) => {
        if (err) return res.send({ error: err });
        const policie = _.filter(
          response,
          o => o.id === req.params.policiesId
        )[0];
        if (!policie) res.send({ error: "the police not exist" });
        getClients((err, response) => {
          const user = _.filter(response, o => o.id === policie.clientId)[0];
          if (user) res.send(user);
          else res.send({ error: "the police not have client" });
        });
      });
    });
  } catch (err) {
    next(err);
  }
});
module.exports = router;

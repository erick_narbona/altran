const getClients = require("../services/getClients").get;
const express = require("express");
const router = express.Router();
const _ = require("underscore");

router.get("/api/v1/client/:id", (req, res, next) => {
  try {
    getClients((err, response) => {
      if (err) return res.send({ error: err });
      res.send(_.filter(response, obj => obj.id === req.params.id)[0]);
    });
  } catch (error) {
    next(error);
  }
});

router.get("/api/v1/client", (req, res, next) => {
  getClients((err, response) => {
    if (err) return res.send({ error: err });
    const name = req.query.name;
    if (name) {
      res.send(
        _.filter(
          response,
          obj => o.name.toLowerCase().indexOf(name.toLocaleLowerCase()) >= 0
        )
      );
    } else {
      res.send(response);
    }
  });
});

module.exports = router;

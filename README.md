# Altran 


This project include the following frameworks:

  - Node.js
  - Express.js
  - underscore.js

## Tech

Altran uses a number of open source projects to work properly:


* [node.js](https://nodejs.org/) - evented I/O for the backend
* [Express](http://expressjs.com/) - fast node.js network app framework [@tjholowaychuk]
* [underscore](http://underscorejs.org/) - functional programming helpers

## Installation

Altran requires [Node.js](https://nodejs.org/) v8+ to run.

Install the dependencies and start the server.

```sh
$ cd Altran
$ npm install -d
$ node app
```
## End Points
### Clients
#### Get All Clients 
- Get all clients
- endpoint http://localhost:3000/api/v1/client

Example:
```sh
$ curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/v1/client
```
Returns
 -return array of clients

```javascript
 [
   {
    "id": "a0ece5db-cd14-4f21-812f-966633e7be86",
    "name": "Britney",
    "email": "britneyblankenship@quotezart.com",
    "role": "admin"
  },
  {
    "id": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
    "name": "Manning",
    "email": "manningblankenship@quotezart.com",
    "role": "admin"
  },
  {
    "id": "a3b8d425-2b60-4ad7-becc-bedf2ef860bd",
    "name": "Barnett",
    "email": "barnettblankenship@quotezart.com",
    "role": "user"
  },
  {
    "id": "44e44268-dce8-4902-b662-1b34d2c10b8e",
    "name": "Merrill",
    "email": "merrillblankenship@quotezart.com",
    "role": "user"
  },
  {....}
]
```
 #### Get Client

 - Get client by id
 - endpoint http://localhost:3000/api/v1/client/:id
 - [param] id

Example:
```sh
$ curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/v1/client/0178914c-548b-4a4c-b918-47d6a391530c
```
Returns
 -return array of clients
```json
  {
    "id": "0178914c-548b-4a4c-b918-47d6a391530c",
    "name": "Whitley",
    "email": "whitleyblankenship@quotezart.com",
    "role": "admin"
  }
```
### Policies
#### Get Policies by client name

 - Get Policies by client name only if user is admin
 - endpoint http://localhost:3000/api/v1/policies/client/:name
 - [param] name  client name
 - [param] authUserId id of user (send by query string)

Example:
```sh
$ curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/v1/policies/client/Manning?authUserId=0178914c-548b-4a4c-b918-47d6a391530c
```
Returns
 -return array of policies
```json
  [
    {
        "id": "64cceef9-3a01-49ae-a23b-3761b604800b",
        "amountInsured": 1825.89,
        "email": "inesblankenship@quotezart.com",
        "inceptionDate": "2016-06-01T03:33:32Z",
        "installmentPayment": true,
        "clientId": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb"
    },
    {
        "id": "56b415d6-53ee-4481-994f-4bffa47b5239",
        "amountInsured": 2301.98,
        "email": "inesblankenship@quotezart.com",
        "inceptionDate": "2014-12-01T05:53:13Z",
        "installmentPayment": false,
        "clientId": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb"
    },
    {
        "id": "5a72ae47-d077-4f74-9166-56a6577e31b9",
        "amountInsured": 751.67,
        "email": "inesblankenship@quotezart.com",
        "inceptionDate": "2015-08-05T04:05:01Z",
        "installmentPayment": true,
        "clientId": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb"
    },
    {....}
  ]
```
#### Get client by policies id

 - Get Policies by user name only if user is admin
 - endpoint http://localhost:3000/api/v1/policies/:name
 - [param] id  policies id
 - [param] authUserId id of user (send by query string)

Example:
```sh
$ curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:3000/api/v1/policies/64cceef9-3a01-49ae-a23b-3761b604800b?authUserId=e8fd159b-57c4-4d36-9bd7-a59ca13057bb
```
Returns
 -return array of policies
```json
  {
    "id": "e8fd159b-57c4-4d36-9bd7-a59ca13057bb",
    "name": "Manning",
    "email": "manningblankenship@quotezart.com",
    "role": "admin"
  }
```
